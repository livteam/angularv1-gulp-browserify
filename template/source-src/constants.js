const AppSettings = {
    appTitle: 'Example Application',
    version: require('../package.json').version
}

export default AppSettings