export const DeclareCtrl = function (directory) {
    const controllersModule = angular.module('app.controllers', [])

    Object.keys(directory).forEach((key) => {
        return controllersModule.controller(directory[key].name, directory[key].fn)
    })
}

export const DeclareDirective = function (directory) {
    const directivesModule = angular.module('app.directives', [])

    Object.keys(directory).forEach((key) => {
        return directivesModule.directive(directory[key].name, directory[key].fn)
    })
}