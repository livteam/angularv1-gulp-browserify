function HomeCtrl($scope) {
    const vm = this
    
    vm.title = "Welcome to Your AngularJS App"
}

export default {
    name: 'HomeCtrl',
    fn: HomeCtrl
}