function HomeDirective() {
    return {
        restrict: 'EA',
        templateUrl: 'views/directives/home.html',
        scope: {
            title: '@',
            message: '@clickMessage'
        },
        link: (scope, element) => {
            element.on('click', () => {
                window.alert('Element clicked: ' + scope.message);
            })
        }
    }
}

export default {
    name: 'homeDirective',
    fn: HomeDirective
};