function routers($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider) {

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    })

    $stateProvider
        .state('Home', {
            url: '/',
            controller: 'HomeCtrl as home',
            templateUrl: 'views/home.html',
            title: 'Home',
            resolve: {
                loadPlugin: ($ocLazyLoad) => $ocLazyLoad.load(['views/css/home.css'])
            }
        })

    $urlRouterProvider.otherwise('/')

}

export default routers