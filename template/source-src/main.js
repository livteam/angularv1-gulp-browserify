// Dependências do NodeJS
import angular from 'angular'
import uiRouter from "@uirouter/angularjs"
import oclazyload from 'oclazyload'
import controllers from './controllers'
import directives from './directives'

import jQuery from 'jquery/dist/jquery.slim'

// Dependências Externas
import constants from './constants'
import routers from './router-config'
import init from './init-config'

// Expondo Global
window.$ = jQuery

// Iniciar o aplicativo
const App = angular.module('myApp', [uiRouter, oclazyload])

angular.element(document).ready( () => {
    angular.bootstrap(document, ['myApp'])
})

// Bootstrap Application
Object.keys(controllers).forEach((key) => {
    App.controller(controllers[key].name, controllers[key].fn);
})
Object.keys(directives).forEach((key) => {
    App.directive(directives[key].name, directives[key].fn);
})
App.constant('AppSettings', constants)
App.config(routers)
App.run(init)

export default App