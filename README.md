# angularV1-gulp-browserify

> A full-featured Browserify + `angular (uglify { mangle: false })` instalado com `Gulp` para Automatizar tarefas durante o Projeto.

> Este Template usa a versão compatível do `Angular 1.6.x`.

[TOC]

## Requisitos

Foi utilizado o [Visual Studio Code](https://code.visualstudio.com/) para criação desse template, usando em base do **Vue-cli** usando a arquitetura de um dos templates oficiais, o [browserify-simple](https://github.com/vuejs-templates/browserify-simple). Acessado no dia 28 de Agosto de 2017.

> **Notas:**
>
> - [NODEJS](https://nodejs.org/): v6.11.2
> - [YARN](https://yarnpkg.com): v0.27.5
>
> **Obs.:**
>
> - [GULP-CLI](https://github.com/gulpjs/gulp/tree/4.0): v1.4.0 (Projeto Baseado no `Gulp v4.0`)

### Use

Este é um Template para Projetos mas sem suporte ainda para o [angular-cli](https://cli.angular.io).

```
$ git clone https://iLeonardo_Carvalho@bitbucket.org/livteam/angularv1-gulp-browserify.git
$ cd angularv1-gulp-browserify/template
$ npm install
$ npm run dev
```

### Observação

Necessário substituir os seguintes valores do projeto:

```
Nome do Projeto: {{name}}
Nome do Autor: {{author}}
Descrição do Projeto: {{description}}
```

### Incluindo

- `npm run serve`: Gerar um servidor local para testes.
- `npm run dev`: Produção para construir HTML/CSS/JS minificados.
- `npm run build`: Compilação e Execução do projeto.

### Customização

Provavelmente será necessário adicionar outras bibliotecas para atender a demanda. Exemplo:

- Biblioteca adicional do AngularJS;

- Configurando EsLint `.eslintrc`.

- Adicionando seu pre-processsor preferido do CSS, por exemplo:

```
yarn add gulp-less
```

  Configurar no `gulpfile.js`:

```javascript
var less = require('gulp-less'); // Compila o LESS para CSS

var paths = {
  ...
  'less': './source-less/*.less/',
  ...
}

function Less() {
  return gulp.src(paths.less)
    .pipe(plumber({
      errorHandler: reportError
    }))
    .pipe(less())
    .pipe(plumber.stop())
    .pipe(gulp.dest('./source-css/'))
    .pipe(notify({
      onLast: true,
      title: 'Gulp',
      subtitle: 'success',
      message: 'Compilado LESS para CSS com sucesso',
      sound: 'Pop'
    }));
}

function Whatch() {
  ...
  gulp.watch(paths.less, gulp.parallel(Less));
  ...
}

...
exports.Less = Less;
...

var styles = gulp.series(Less, Css),
```

- Crie a pasta `source-less` dentro de seu projeto e comece a usufruir pelo `Gulp`.

## Direitos autorais e Licença

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **MIT**](https://opensource.org/licenses/MIT).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
